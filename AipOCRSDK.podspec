#
# Be sure to run `pod lib lint AipOCRSDK.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name      = 'AipOCRSDK'
    s.version   = '3.0.3'
    s.summary   = 'AipOCRSDK.'
    s.homepage  = 'https://gitlab.com/rogerkuu/baiduocr'
    s.license   = 'Apache License, Version 2.0'
    s.author    = { "mjgu" => "mjgu@lpht.com.cn" }
    s.source    = { :git => 'https://gitlab.com/rogerkuu/baiduocr.git', :tag => s.version.to_s }

    s.platform                = :ios
    s.ios.deployment_target   = '9.0'

    s.ios.deployment_target = '9.0'
    s.subspec 'AipBase' do |b|
      b.vendored_frameworks ='release/AipBase.framework'
    end
  
    s.subspec 'AipOcrSdk' do |s|
      s.vendored_frameworks ='release/AipOcrSdk.framework'
    end
  
    s.subspec 'IdcardQuality' do |i|
      i.vendored_frameworks ='release/IdcardQuality.framework'
    end

    s.pod_target_xcconfig = {
    'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
  }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end