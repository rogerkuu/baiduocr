#
# Be sure to run `pod lib lint AipOCRSDK_debug.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'AipOCRSDK'
  s.version          = '3.0.3'
  s.summary          = 'AipOCRSDK-debug...'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
AipOcrSdk dynamic framework -debug
                       DESC

  s.homepage         = 'https://gitlab.com/rogerkuu/baiduocr'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mjgu' => 'mjgu@lpht.com.cn' }
  s.source           = { :git => 'https://gitlab.com/rogerkuu/baiduocr.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.subspec 'AipBase' do |b|
      b.vendored_frameworks ='debug/AipBase.framework'
  end
  
  s.subspec 'AipOcrSdk' do |s|
      s.vendored_frameworks ='debug/AipOcrSdk.framework'
  end
  
  s.subspec 'IdcardQuality' do |i|
      i.vendored_frameworks ='debug/IdcardQuality.framework'
  end
end